package com.surfalsur.resetario.web.rest;

import com.surfalsur.resetario.ResetarioApp;

import com.surfalsur.resetario.domain.IngredientItem;
import com.surfalsur.resetario.repository.IngredientItemRepository;
import com.surfalsur.resetario.repository.search.IngredientItemSearchRepository;
import com.surfalsur.resetario.service.dto.BaseCodeLabelDTO;
import com.surfalsur.resetario.service.dto.IngredientItemDTO;
import com.surfalsur.resetario.service.mapper.IngredientItemMapper;
import com.surfalsur.resetario.web.rest.errors.ExceptionTranslator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.print.attribute.standard.Media;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.surfalsur.resetario.domain.enumeration.MediationUnit;
/**
 * Test class for the IngredientItemResource REST controller.
 *
 * @see IngredientItemResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ResetarioApp.class)
public class IngredientItemResourceIntTest {

    private static final Integer DEFAULT_QUANTITY = 0;
    private static final Integer UPDATED_QUANTITY = 1;

    private static final MediationUnit DEFAULT_MEDIATION_UNIT = MediationUnit.GRM;
    private static final MediationUnit UPDATED_MEDIATION_UNIT = MediationUnit.KILOS;

    @Autowired
    private IngredientItemRepository ingredientItemRepository;

    @Autowired
    private IngredientItemMapper ingredientItemMapper;

    @Autowired
    private IngredientItemSearchRepository ingredientItemSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restIngredientItemMockMvc;

    private IngredientItem ingredientItem;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        IngredientItemResource ingredientItemResource = new IngredientItemResource(ingredientItemRepository, ingredientItemMapper, ingredientItemSearchRepository);
        this.restIngredientItemMockMvc = MockMvcBuilders.standaloneSetup(ingredientItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IngredientItem createEntity(EntityManager em) {
        IngredientItem ingredientItem = new IngredientItem()
            .quantity(DEFAULT_QUANTITY)
            .mediationUnit(DEFAULT_MEDIATION_UNIT);
        return ingredientItem;
    }

    @Before
    public void initTest() {
        ingredientItemSearchRepository.deleteAll();
        ingredientItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createIngredientItem() throws Exception {
        int databaseSizeBeforeCreate = ingredientItemRepository.findAll().size();

        // Create the IngredientItem
        IngredientItemDTO ingredientItemDTO = ingredientItemMapper.toDto(ingredientItem);
        restIngredientItemMockMvc.perform(post("/api/ingredient-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ingredientItemDTO)))
            .andExpect(status().isCreated());

        // Validate the IngredientItem in the database
        List<IngredientItem> ingredientItemList = ingredientItemRepository.findAll();
        assertThat(ingredientItemList).hasSize(databaseSizeBeforeCreate + 1);
        IngredientItem testIngredientItem = ingredientItemList.get(ingredientItemList.size() - 1);
        assertThat(testIngredientItem.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testIngredientItem.getMediationUnit()).isEqualTo(DEFAULT_MEDIATION_UNIT);

        // Validate the IngredientItem in Elasticsearch
        IngredientItem ingredientItemEs = ingredientItemSearchRepository.findOne(testIngredientItem.getId());
        assertThat(ingredientItemEs).isEqualToComparingFieldByField(testIngredientItem);
    }

    @Test
    @Transactional
    public void createIngredientItemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ingredientItemRepository.findAll().size();

        // Create the IngredientItem with an existing ID
        ingredientItem.setId(1L);
        IngredientItemDTO ingredientItemDTO = ingredientItemMapper.toDto(ingredientItem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIngredientItemMockMvc.perform(post("/api/ingredient-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ingredientItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<IngredientItem> ingredientItemList = ingredientItemRepository.findAll();
        assertThat(ingredientItemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllIngredientItems() throws Exception {
        // Initialize the database
        ingredientItemRepository.saveAndFlush(ingredientItem);

        // Get all the ingredientItemList
        restIngredientItemMockMvc.perform(get("/api/ingredient-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ingredientItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].mediationUnit").value(hasItem(DEFAULT_MEDIATION_UNIT.toString())));
    }

    @Test
    @Transactional
    public void getIngredientItem() throws Exception {
        // Initialize the database
        ingredientItemRepository.saveAndFlush(ingredientItem);

        // Get the ingredientItem
        restIngredientItemMockMvc.perform(get("/api/ingredient-items/{id}", ingredientItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ingredientItem.getId().intValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY))
            .andExpect(jsonPath("$.mediationUnit").value(DEFAULT_MEDIATION_UNIT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingIngredientItem() throws Exception {
        // Get the ingredientItem
        restIngredientItemMockMvc.perform(get("/api/ingredient-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIngredientItem() throws Exception {
        // Initialize the database
        ingredientItemRepository.saveAndFlush(ingredientItem);
        ingredientItemSearchRepository.save(ingredientItem);
        int databaseSizeBeforeUpdate = ingredientItemRepository.findAll().size();

        // Update the ingredientItem
        IngredientItem updatedIngredientItem = ingredientItemRepository.findOne(ingredientItem.getId());
        updatedIngredientItem
            .quantity(UPDATED_QUANTITY)
            .mediationUnit(UPDATED_MEDIATION_UNIT);
        IngredientItemDTO ingredientItemDTO = ingredientItemMapper.toDto(updatedIngredientItem);

        restIngredientItemMockMvc.perform(put("/api/ingredient-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ingredientItemDTO)))
            .andExpect(status().isOk());

        // Validate the IngredientItem in the database
        List<IngredientItem> ingredientItemList = ingredientItemRepository.findAll();
        assertThat(ingredientItemList).hasSize(databaseSizeBeforeUpdate);
        IngredientItem testIngredientItem = ingredientItemList.get(ingredientItemList.size() - 1);
        assertThat(testIngredientItem.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testIngredientItem.getMediationUnit()).isEqualTo(UPDATED_MEDIATION_UNIT);

        // Validate the IngredientItem in Elasticsearch
        IngredientItem ingredientItemEs = ingredientItemSearchRepository.findOne(testIngredientItem.getId());
        assertThat(ingredientItemEs).isEqualToComparingFieldByField(testIngredientItem);
    }

    @Test
    @Transactional
    public void updateNonExistingIngredientItem() throws Exception {
        int databaseSizeBeforeUpdate = ingredientItemRepository.findAll().size();

        // Create the IngredientItem
        IngredientItemDTO ingredientItemDTO = ingredientItemMapper.toDto(ingredientItem);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restIngredientItemMockMvc.perform(put("/api/ingredient-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ingredientItemDTO)))
            .andExpect(status().isCreated());

        // Validate the IngredientItem in the database
        List<IngredientItem> ingredientItemList = ingredientItemRepository.findAll();
        assertThat(ingredientItemList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteIngredientItem() throws Exception {
        // Initialize the database
        ingredientItemRepository.saveAndFlush(ingredientItem);
        ingredientItemSearchRepository.save(ingredientItem);
        int databaseSizeBeforeDelete = ingredientItemRepository.findAll().size();

        // Get the ingredientItem
        restIngredientItemMockMvc.perform(delete("/api/ingredient-items/{id}", ingredientItem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean ingredientItemExistsInEs = ingredientItemSearchRepository.exists(ingredientItem.getId());
        assertThat(ingredientItemExistsInEs).isFalse();

        // Validate the database is empty
        List<IngredientItem> ingredientItemList = ingredientItemRepository.findAll();
        assertThat(ingredientItemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchIngredientItem() throws Exception {
        // Initialize the database
        ingredientItemRepository.saveAndFlush(ingredientItem);
        ingredientItemSearchRepository.save(ingredientItem);

        // Search the ingredientItem
        restIngredientItemMockMvc.perform(get("/api/_search/ingredient-items?query=id:" + ingredientItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ingredientItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].mediationUnit").value(hasItem(DEFAULT_MEDIATION_UNIT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IngredientItem.class);
        IngredientItem ingredientItem1 = new IngredientItem();
        ingredientItem1.setId(1L);
        IngredientItem ingredientItem2 = new IngredientItem();
        ingredientItem2.setId(ingredientItem1.getId());
        assertThat(ingredientItem1).isEqualTo(ingredientItem2);
        ingredientItem2.setId(2L);
        assertThat(ingredientItem1).isNotEqualTo(ingredientItem2);
        ingredientItem1.setId(null);
        assertThat(ingredientItem1).isNotEqualTo(ingredientItem2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(IngredientItemDTO.class);
        IngredientItemDTO ingredientItemDTO1 = new IngredientItemDTO();
        ingredientItemDTO1.setId(1L);
        IngredientItemDTO ingredientItemDTO2 = new IngredientItemDTO();
        assertThat(ingredientItemDTO1).isNotEqualTo(ingredientItemDTO2);
        ingredientItemDTO2.setId(ingredientItemDTO1.getId());
        assertThat(ingredientItemDTO1).isEqualTo(ingredientItemDTO2);
        ingredientItemDTO2.setId(2L);
        assertThat(ingredientItemDTO1).isNotEqualTo(ingredientItemDTO2);
        ingredientItemDTO1.setId(null);
        assertThat(ingredientItemDTO1).isNotEqualTo(ingredientItemDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(ingredientItemMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(ingredientItemMapper.fromId(null)).isNull();
    }


    @Test
    public void shouldReturnAllMediationUnits() throws Exception {
        // Initialize the database
        restIngredientItemMockMvc.perform(get("/api/mediation-units"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].label").value(hasItem("Gramos")));


    }
}
