package com.surfalsur.resetario.service.mapper;

import com.surfalsur.resetario.ResetarioApp;
import com.surfalsur.resetario.domain.Category;
import com.surfalsur.resetario.domain.Ingredient;
import com.surfalsur.resetario.domain.IngredientItem;
import com.surfalsur.resetario.domain.Recipe;
import com.surfalsur.resetario.domain.enumeration.MediationUnit;
import com.surfalsur.resetario.service.dto.RecipeDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ResetarioApp.class)
public class RecipeMapperTest {


    @Autowired
    private RecipeMapperImpl recipeMapper;

    @Test
    public void shoulMapRecipeToRecipeDTO() {

        Recipe recipe = new Recipe();
        recipe.setId(1l);
        recipe.setName("receta1");
        recipe.setInstructions("instruc....");

        Set<IngredientItem> ingredientItems = new HashSet<IngredientItem>(1);
        IngredientItem item = new IngredientItem();
        item.setId(1l);
        item.setRecipe(recipe);
        item.setMediationUnit(MediationUnit.KILOS);
        item.setQuantity(2);

        Ingredient ingredient = new Ingredient();
        ingredient.setId(3l);
        ingredient.setName("Peixe");

        Category cat = new Category();
        cat.setId(6l);
        cat.setName("No Vegetal");

        ingredient.setCategory(cat);

        item.setIngredient(ingredient);
        ingredientItems.add(item);

        recipe.setIngredientItems(ingredientItems);

        RecipeDTO result = recipeMapper.toDto(recipe);

        Assert.assertTrue(result.getIngredientItems().stream().findFirst().get().getIngredient().getName().equals("Peixe"));

    }

}
