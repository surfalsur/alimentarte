import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ResetarioTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { IngredientItemDetailComponent } from '../../../../../../main/webapp/app/entities/ingredient-item/ingredient-item-detail.component';
import { IngredientItemService } from '../../../../../../main/webapp/app/entities/ingredient-item/ingredient-item.service';
import { IngredientItem } from '../../../../../../main/webapp/app/entities/ingredient-item/ingredient-item.model';

describe('Component Tests', () => {

    describe('IngredientItem Management Detail Component', () => {
        let comp: IngredientItemDetailComponent;
        let fixture: ComponentFixture<IngredientItemDetailComponent>;
        let service: IngredientItemService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ResetarioTestModule],
                declarations: [IngredientItemDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    IngredientItemService,
                    JhiEventManager
                ]
            }).overrideTemplate(IngredientItemDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(IngredientItemDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IngredientItemService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new IngredientItem(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.ingredientItem).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
