import {BaseEntity} from './base-entity';

export class BaseNameEntity {

    constructor(
        public code?: string,
        public label?: string
    ) {
    }
}