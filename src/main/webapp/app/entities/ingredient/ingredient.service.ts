import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Ingredient } from './ingredient.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class IngredientService {

    private resourceUrl = 'api/ingredients';
    private resourceSearchUrl = 'api/_search/ingredients';

    constructor(private http: Http) { }

    create(ingredient: Ingredient): Observable<Ingredient> {
        const copy = this.convert(ingredient);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(ingredient: Ingredient): Observable<Ingredient> {
        const copy = this.convert(ingredient);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Ingredient> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(ingredient: Ingredient): Ingredient {
        const copy: Ingredient = Object.assign({}, ingredient);
        return copy;
    }
}
