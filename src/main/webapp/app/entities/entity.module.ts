import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { ResetarioIngredientModule } from './ingredient/ingredient.module';
import { ResetarioCategoryModule } from './category/category.module';
import { ResetarioIngredientItemModule } from './ingredient-item/ingredient-item.module';
import { ResetarioRecipeModule } from './recipe/recipe.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        ResetarioIngredientModule,
        ResetarioCategoryModule,
        ResetarioIngredientItemModule,
        ResetarioRecipeModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ResetarioEntityModule {}
