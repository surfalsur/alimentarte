import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ResetarioSharedModule } from '../../shared';
import {
    RecipeService,
    RecipePopupService,
    RecipeComponent,
    RecipeDetailComponent,
    RecipeDialogComponent,
    RecipePopupComponent,
    RecipeDeletePopupComponent,
    RecipeDeleteDialogComponent,
    recipeRoute,
    recipePopupRoute,
    RecipeResolvePagingParams
} from './';

import {IngredientItemService} from '../ingredient-item/ingredient-item.service';
import {AutoCompleteModule} from 'primeng/primeng';
import { RecipeIngredientListComponent } from './recipe-ingredient-list/recipe-ingredient-list.component';
import { RecipeIngredientItemComponent } from './recipe-ingredient-item/recipe-ingredient-item.component';

const ENTITY_STATES = [
    ...recipeRoute,
    ...recipePopupRoute,
];

@NgModule({
    imports: [
        ResetarioSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        AutoCompleteModule
    ],
    declarations: [
        RecipeComponent,
        RecipeDetailComponent,
        RecipeDialogComponent,
        RecipeDeleteDialogComponent,
        RecipePopupComponent,
        RecipeDeletePopupComponent,
        RecipeIngredientListComponent,
        RecipeIngredientItemComponent,
    ],
    entryComponents: [
        RecipeComponent,
        RecipeDialogComponent,
        RecipePopupComponent,
        RecipeDeleteDialogComponent,
        RecipeDeletePopupComponent,
        RecipeIngredientListComponent,
        RecipeIngredientItemComponent,
    ],
    providers: [
        RecipeService,
        RecipePopupService,
        RecipeResolvePagingParams,
        IngredientItemService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ResetarioRecipeModule {}
