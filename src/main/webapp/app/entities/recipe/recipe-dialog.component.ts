import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Recipe } from './recipe.model';
import { RecipePopupService } from './recipe-popup.service';
import { RecipeService } from './recipe.service';
import { RecipeIngredientListComponent } from './recipe-ingredient-list/recipe-ingredient-list.component';

@Component({
    selector: 'jhi-recipe-dialog',
    templateUrl: './recipe-dialog.component.html'
})
export class RecipeDialogComponent implements OnInit {

    recipe: Recipe;
    authorities: any[];
    isSaving: boolean;

    @ViewChild(RecipeIngredientListComponent)
    private recipeIngredientListComponent: RecipeIngredientListComponent;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private recipeService: RecipeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        this.recipe.ingredientItems = this.recipeIngredientListComponent.ingredientItems;
        if (this.recipe.id !== undefined) {
            this.subscribeToSaveResponse(
                this.recipeService.update(this.recipe), false);
        } else {
            this.subscribeToSaveResponse(
                this.recipeService.create(this.recipe), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<Recipe>, isCreated: boolean) {
        result.subscribe((res: Recipe) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Recipe, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'resetarioApp.recipe.created'
            : 'resetarioApp.recipe.updated',
            { param : result.id }, null);

        this.eventManager.broadcast({ name: 'recipeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-recipe-popup',
    template: ''
})
export class RecipePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private recipePopupService: RecipePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.recipePopupService
                    .open(RecipeDialogComponent, params['id']);
            } else {
                this.modalRef = this.recipePopupService
                    .open(RecipeDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
