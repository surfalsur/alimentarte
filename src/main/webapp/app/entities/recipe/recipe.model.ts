import { BaseEntity } from './../../shared';

export class Recipe implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public instructions?: string,
        public ingredientItems?: BaseEntity[],
    ) {
    }
}
