import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { Principal, ResponseWrapper } from '../../../shared';
import { IngredientItemService } from '../../ingredient-item/ingredient-item.service';
import { IngredientService } from '../../ingredient/ingredient.service';
import { BaseNameEntity } from '../../../shared/model/base-name-entity';
import { JhiAlertService } from 'ng-jhipster';
import { Ingredient} from '../../ingredient/ingredient.model';
import { IngredientItem } from '../../ingredient-item/ingredient-item.model';

@Component({
  selector: 'jhi-recipe-ingredient-item',
  templateUrl: './recipe-ingredient-item.component.html',
  styles: []
})
export class RecipeIngredientItemComponent implements OnInit {

  filteredIngredients: Ingredient[];
  mediationUnits: BaseNameEntity[];
  @Input() ingredientItem: IngredientItem;

  constructor(private ingredientItemService: IngredientItemService,
    private alertService: JhiAlertService,
    private ingredientService: IngredientService) {}

  ngOnInit() {
    this.loadMediationUnits();
  }

  handleDropdownClick() {
    this.filteredIngredients = [];
    return this.ingredientService.query()
      .subscribe(
      (res: ResponseWrapper) => {
            this.filteredIngredients = res.json;
      }
      ,
      (res: ResponseWrapper) => this.alertService.error(res.json)
      );
  }

  loadMediationUnits() {
    return this.ingredientItemService.searchMediationUnits()
      .subscribe(
      (res: ResponseWrapper) => this.mediationUnits = res.json,
      (res: ResponseWrapper) => this.alertService.error(res.json.message, null, null));
  }

  searchIngredients(event) {
    this.filteredIngredients = [];
    return this.ingredientService.search({
        query: event.query
    }).subscribe(
      (res: ResponseWrapper) => {
            this.filteredIngredients = res.json;
      }
      ,
      (res: ResponseWrapper) => this.alertService.error(res.json)
      );
  }

}
