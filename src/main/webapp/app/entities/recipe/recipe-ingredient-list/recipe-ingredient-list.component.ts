import { Component, OnInit, OnDestroy, ViewChild, ComponentFactoryResolver, ComponentRef, ComponentFactory, ViewContainerRef, OnChanges, Input, EventEmitter } from '@angular/core';

import { IngredientItem } from '../../ingredient-item/ingredient-item.model';
import { Ingredient} from '../../ingredient/ingredient.model';
import { IngredientItemService } from '../../ingredient-item/ingredient-item.service';
import { IngredientService } from '../../ingredient/ingredient.service';
import { Principal, ResponseWrapper } from '../../../shared';
import {BaseNameEntity} from '../../../shared/model/base-name-entity';
import {Recipe} from '../recipe.model';
import {JhiAlertService} from 'ng-jhipster';
import {RecipeIngredientItemComponent} from '../recipe-ingredient-item/recipe-ingredient-item.component';

@Component({
    selector: 'jhi-recipe-ingredient-list',
    templateUrl: './recipe-ingredient-list.component.html',
    styleUrls: ['./recipe-ingredient-list.component.css']
})

export class RecipeIngredientListComponent implements OnInit, OnDestroy {

    @Input() ingredientItems: IngredientItem [];
    ingredientItem: IngredientItem;

    constructor(
         private ingredientItemService: IngredientItemService,
         private alertService: JhiAlertService,
         private ingredientService: IngredientService
    ) {}

    addIngredientItem(event) {
        event.stopPropagation();
        this.ingredientItems.push(new IngredientItem());
    }

    ngOnInit() {
        if (!this.ingredientItems || this.ingredientItems.length === 0) {
            this.ingredientItems = [];
            this.ingredientItems.push(new IngredientItem());
        }
    }

    ngOnDestroy() {
        // this.componentRef.destroy();
    }
}
