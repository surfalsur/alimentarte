import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { IngredientItem } from './ingredient-item.model';
import { IngredientItemService } from './ingredient-item.service';

@Component({
    selector: 'jhi-ingredient-item-detail',
    templateUrl: './ingredient-item-detail.component.html'
})
export class IngredientItemDetailComponent implements OnInit, OnDestroy {

    ingredientItem: IngredientItem;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ingredientItemService: IngredientItemService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInIngredientItems();
    }

    load(id) {
        this.ingredientItemService.find(id).subscribe((ingredientItem) => {
            this.ingredientItem = ingredientItem;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInIngredientItems() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ingredientItemListModification',
            (response) => this.load(this.ingredientItem.id)
        );
    }
}
