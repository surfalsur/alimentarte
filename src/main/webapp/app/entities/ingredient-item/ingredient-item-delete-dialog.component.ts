import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { IngredientItem } from './ingredient-item.model';
import { IngredientItemPopupService } from './ingredient-item-popup.service';
import { IngredientItemService } from './ingredient-item.service';

@Component({
    selector: 'jhi-ingredient-item-delete-dialog',
    templateUrl: './ingredient-item-delete-dialog.component.html'
})
export class IngredientItemDeleteDialogComponent {

    ingredientItem: IngredientItem;

    constructor(
        private ingredientItemService: IngredientItemService,
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ingredientItemService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ingredientItemListModification',
                content: 'Deleted an ingredientItem'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success('resetarioApp.ingredientItem.deleted', { param : id }, null);
    }
}

@Component({
    selector: 'jhi-ingredient-item-delete-popup',
    template: ''
})
export class IngredientItemDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ingredientItemPopupService: IngredientItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.ingredientItemPopupService
                .open(IngredientItemDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
