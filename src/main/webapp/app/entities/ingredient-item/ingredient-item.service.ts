import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { IngredientItem } from './ingredient-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class IngredientItemService {

    private resourceUrl = 'api/ingredient-items';
    private resourceSearchUrl = 'api/_search/ingredient-items';
    private resourceMediationUnitURl = 'api/mediation-units';

    constructor(private http: Http) { }

    create(ingredientItem: IngredientItem): Observable<IngredientItem> {
        const copy = this.convert(ingredientItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(ingredientItem: IngredientItem): Observable<IngredientItem> {
        const copy = this.convert(ingredientItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<IngredientItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    searchMediationUnits(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceMediationUnitURl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(ingredientItem: IngredientItem): IngredientItem {
        const copy: IngredientItem = Object.assign({}, ingredientItem);
        return copy;
    }

}
