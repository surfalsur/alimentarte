import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { IngredientItem } from './ingredient-item.model';
import { IngredientItemService } from './ingredient-item.service';

@Injectable()
export class IngredientItemPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private ingredientItemService: IngredientItemService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.ingredientItemService.find(id).subscribe((ingredientItem) => {
                this.ingredientItemModalRef(component, ingredientItem);
            });
        } else {
            return this.ingredientItemModalRef(component, new IngredientItem());
        }
    }

    ingredientItemModalRef(component: Component, ingredientItem: IngredientItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.ingredientItem = ingredientItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
