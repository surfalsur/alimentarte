import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IngredientItem } from './ingredient-item.model';
import { IngredientItemPopupService } from './ingredient-item-popup.service';
import { IngredientItemService } from './ingredient-item.service';
import { Ingredient, IngredientService } from '../ingredient';
import { Recipe, RecipeService } from '../recipe';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-ingredient-item-dialog',
    templateUrl: './ingredient-item-dialog.component.html'
})
export class IngredientItemDialogComponent implements OnInit {

    ingredientItem: IngredientItem;
    authorities: any[];
    isSaving: boolean;

    ingredients: Ingredient[];

    recipes: Recipe[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private ingredientItemService: IngredientItemService,
        private ingredientService: IngredientService,
        private recipeService: RecipeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.ingredientService.query()
            .subscribe((res: ResponseWrapper) => { this.ingredients = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.recipeService.query()
            .subscribe((res: ResponseWrapper) => { this.recipes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.ingredientItem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.ingredientItemService.update(this.ingredientItem), false);
        } else {
            this.subscribeToSaveResponse(
                this.ingredientItemService.create(this.ingredientItem), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<IngredientItem>, isCreated: boolean) {
        result.subscribe((res: IngredientItem) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: IngredientItem, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'resetarioApp.ingredientItem.created'
            : 'resetarioApp.ingredientItem.updated',
            { param : result.id }, null);

        this.eventManager.broadcast({ name: 'ingredientItemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackIngredientById(index: number, item: Ingredient) {
        return item.id;
    }

    trackRecipeById(index: number, item: Recipe) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-ingredient-item-popup',
    template: ''
})
export class IngredientItemPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ingredientItemPopupService: IngredientItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.ingredientItemPopupService
                    .open(IngredientItemDialogComponent, params['id']);
            } else {
                this.modalRef = this.ingredientItemPopupService
                    .open(IngredientItemDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
