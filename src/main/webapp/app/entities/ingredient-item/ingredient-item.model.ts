import { BaseEntity } from './../../shared';
import { Ingredient } from '../ingredient/ingredient.model';

const enum MediationUnit {
    'GRM',
    'KILOS',
    'LITROS',
    'CM_CUBICOS',
    'UNIDADES',
    'CUCHARADAS'
}

export class IngredientItem implements BaseEntity {

    constructor(
        public id?: number,
        public quantity?: number,
        public mediationUnit?: MediationUnit,
        public ingredient?: Ingredient,
        public ingredientId?: number,
        public recipeId?: number,
    ) {
    }
}
