import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ResetarioSharedModule } from '../../shared';
import {
    IngredientItemService,
    IngredientItemPopupService,
    IngredientItemComponent,
    IngredientItemDetailComponent,
    IngredientItemDialogComponent,
    IngredientItemPopupComponent,
    IngredientItemDeletePopupComponent,
    IngredientItemDeleteDialogComponent,
    ingredientItemRoute,
    ingredientItemPopupRoute,
    IngredientItemResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...ingredientItemRoute,
    ...ingredientItemPopupRoute,
];

@NgModule({
    imports: [
        ResetarioSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        IngredientItemComponent,
        IngredientItemDetailComponent,
        IngredientItemDialogComponent,
        IngredientItemDeleteDialogComponent,
        IngredientItemPopupComponent,
        IngredientItemDeletePopupComponent,
    ],
    entryComponents: [
        IngredientItemComponent,
        IngredientItemDialogComponent,
        IngredientItemPopupComponent,
        IngredientItemDeleteDialogComponent,
        IngredientItemDeletePopupComponent,
    ],
    providers: [
        IngredientItemService,
        IngredientItemPopupService,
        IngredientItemResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ResetarioIngredientItemModule {}
