import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { IngredientItemComponent } from './ingredient-item.component';
import { IngredientItemDetailComponent } from './ingredient-item-detail.component';
import { IngredientItemPopupComponent } from './ingredient-item-dialog.component';
import { IngredientItemDeletePopupComponent } from './ingredient-item-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class IngredientItemResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const ingredientItemRoute: Routes = [
    {
        path: 'ingredient-item',
        component: IngredientItemComponent,
        resolve: {
            'pagingParams': IngredientItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'resetarioApp.ingredientItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'ingredient-item/:id',
        component: IngredientItemDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'resetarioApp.ingredientItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ingredientItemPopupRoute: Routes = [
    {
        path: 'ingredient-item-new',
        component: IngredientItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'resetarioApp.ingredientItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ingredient-item/:id/edit',
        component: IngredientItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'resetarioApp.ingredientItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ingredient-item/:id/delete',
        component: IngredientItemDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'resetarioApp.ingredientItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
