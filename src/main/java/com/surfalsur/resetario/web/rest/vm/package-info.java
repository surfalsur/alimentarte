/**
 * View Models used by Spring MVC REST controllers.
 */
package com.surfalsur.resetario.web.rest.vm;
