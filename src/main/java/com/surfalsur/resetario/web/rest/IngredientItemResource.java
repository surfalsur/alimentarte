package com.surfalsur.resetario.web.rest;

import com.codahale.metrics.MetricRegistryListener;
import com.codahale.metrics.annotation.Timed;
import com.surfalsur.resetario.domain.IngredientItem;

import com.surfalsur.resetario.domain.enumeration.MediationUnit;
import com.surfalsur.resetario.repository.IngredientItemRepository;
import com.surfalsur.resetario.repository.search.IngredientItemSearchRepository;
import com.surfalsur.resetario.service.dto.BaseCodeLabelDTO;
import com.surfalsur.resetario.web.rest.util.HeaderUtil;
import com.surfalsur.resetario.web.rest.util.PaginationUtil;
import com.surfalsur.resetario.service.dto.IngredientItemDTO;
import com.surfalsur.resetario.service.mapper.IngredientItemMapper;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing IngredientItem.
 */
@RestController
@RequestMapping("/api")
public class IngredientItemResource {

    private final Logger log = LoggerFactory.getLogger(IngredientItemResource.class);

    private static final String ENTITY_NAME = "ingredientItem";

    private final IngredientItemRepository ingredientItemRepository;

    private final IngredientItemMapper ingredientItemMapper;

    private final IngredientItemSearchRepository ingredientItemSearchRepository;

    public IngredientItemResource(IngredientItemRepository ingredientItemRepository, IngredientItemMapper ingredientItemMapper, IngredientItemSearchRepository ingredientItemSearchRepository) {
        this.ingredientItemRepository = ingredientItemRepository;
        this.ingredientItemMapper = ingredientItemMapper;
        this.ingredientItemSearchRepository = ingredientItemSearchRepository;
    }

    /**
     * POST  /ingredient-items : Create a new ingredientItem.
     *
     * @param ingredientItemDTO the ingredientItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ingredientItemDTO, or with status 400 (Bad Request) if the ingredientItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ingredient-items")
    @Timed
    public ResponseEntity<IngredientItemDTO> createIngredientItem(@Valid @RequestBody IngredientItemDTO ingredientItemDTO) throws URISyntaxException {
        log.debug("REST request to save IngredientItem : {}", ingredientItemDTO);
        if (ingredientItemDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new ingredientItem cannot already have an ID")).body(null);
        }
        IngredientItem ingredientItem = ingredientItemMapper.toEntity(ingredientItemDTO);
        ingredientItem = ingredientItemRepository.save(ingredientItem);
        IngredientItemDTO result = ingredientItemMapper.toDto(ingredientItem);
        ingredientItemSearchRepository.save(ingredientItem);
        return ResponseEntity.created(new URI("/api/ingredient-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ingredient-items : Updates an existing ingredientItem.
     *
     * @param ingredientItemDTO the ingredientItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ingredientItemDTO,
     * or with status 400 (Bad Request) if the ingredientItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the ingredientItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ingredient-items")
    @Timed
    public ResponseEntity<IngredientItemDTO> updateIngredientItem(@Valid @RequestBody IngredientItemDTO ingredientItemDTO) throws URISyntaxException {
        log.debug("REST request to update IngredientItem : {}", ingredientItemDTO);
        if (ingredientItemDTO.getId() == null) {
            return createIngredientItem(ingredientItemDTO);
        }
        IngredientItem ingredientItem = ingredientItemMapper.toEntity(ingredientItemDTO);
        ingredientItem = ingredientItemRepository.save(ingredientItem);
        IngredientItemDTO result = ingredientItemMapper.toDto(ingredientItem);
        ingredientItemSearchRepository.save(ingredientItem);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ingredientItemDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ingredient-items : get all the ingredientItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ingredientItems in body
     */
    @GetMapping("/ingredient-items")
    @Timed
    public ResponseEntity<List<IngredientItemDTO>> getAllIngredientItems(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of IngredientItems");
        Page<IngredientItem> page = ingredientItemRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ingredient-items");
        return new ResponseEntity<>(ingredientItemMapper.toDto(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /ingredient-items/:id : get the "id" ingredientItem.
     *
     * @param id the id of the ingredientItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ingredientItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/ingredient-items/{id}")
    @Timed
    public ResponseEntity<IngredientItemDTO> getIngredientItem(@PathVariable Long id) {
        log.debug("REST request to get IngredientItem : {}", id);
        IngredientItem ingredientItem = ingredientItemRepository.findOne(id);
        IngredientItemDTO ingredientItemDTO = ingredientItemMapper.toDto(ingredientItem);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ingredientItemDTO));
    }

    /**
     * DELETE  /ingredient-items/:id : delete the "id" ingredientItem.
     *
     * @param id the id of the ingredientItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ingredient-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteIngredientItem(@PathVariable Long id) {
        log.debug("REST request to delete IngredientItem : {}", id);
        ingredientItemRepository.delete(id);
        ingredientItemSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/ingredient-items?query=:query : search for the ingredientItem corresponding
     * to the query.
     *
     * @param query the query of the ingredientItem search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/ingredient-items")
    @Timed
    public ResponseEntity<List<IngredientItemDTO>> searchIngredientItems(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of IngredientItems for query {}", query);
        Page<IngredientItem> page = ingredientItemSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/ingredient-items");
        return new ResponseEntity<>(ingredientItemMapper.toDto(page.getContent()), headers, HttpStatus.OK);
    }


    /**
     * GET  /mediation-units : get all Mediation Units.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of mediation units in body
     */
    @GetMapping("/mediation-units")
    @Timed
    public ResponseEntity<List<BaseCodeLabelDTO>> getAllMediationUnits() {
        log.debug("REST request to get a page of MediationUnits");

        List<BaseCodeLabelDTO> result = Arrays.asList(MediationUnit.values()).stream().map(mu -> new BaseCodeLabelDTO(mu.name(),mu.getLabel()))
            .collect(Collectors.toList());

        return new ResponseEntity<>(result,HttpStatus.OK);
    }

}
