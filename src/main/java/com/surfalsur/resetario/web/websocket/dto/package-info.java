/**
 * Data Access Objects used by WebSocket services.
 */
package com.surfalsur.resetario.web.websocket.dto;
