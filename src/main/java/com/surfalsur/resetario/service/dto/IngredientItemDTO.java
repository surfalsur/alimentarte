package com.surfalsur.resetario.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.surfalsur.resetario.domain.Ingredient;
import com.surfalsur.resetario.domain.enumeration.MediationUnit;

/**
 * A DTO for the IngredientItem entity.
 */
public class IngredientItemDTO implements Serializable {

    private Long id;

    @Min(value = 0)
    private Integer quantity;

    private MediationUnit mediationUnit;

    private IngredientDTO ingredient;

    private Long recipeId;

    private String recipeName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public MediationUnit getMediationUnit() {
        return mediationUnit;
    }

    public void setMediationUnit(MediationUnit mediationUnit) {
        this.mediationUnit = mediationUnit;
    }

    public IngredientDTO getIngredient() {
        return ingredient;
    }

    public void setIngredient(IngredientDTO ingredient) {
        this.ingredient = ingredient;
    }

    public Long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Long recipeId) {
        this.recipeId = recipeId;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IngredientItemDTO ingredientItemDTO = (IngredientItemDTO) o;
        if(ingredientItemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ingredientItemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IngredientItemDTO{" +
            "id=" + getId() +
            ", quantity='" + getQuantity() + "'" +
            ", mediationUnit='" + getMediationUnit() + "'" +
            "}";
    }
}
