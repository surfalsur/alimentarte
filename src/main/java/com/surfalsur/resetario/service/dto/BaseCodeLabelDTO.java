package com.surfalsur.resetario.service.dto;

import java.io.Serializable;

public class BaseCodeLabelDTO  implements Serializable {

    private String code;
    private String label;

    public BaseCodeLabelDTO(String code,String label) {
        this.code = code;
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

}
