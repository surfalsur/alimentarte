package com.surfalsur.resetario.service.dto;


import java.io.Serializable;
import java.util.*;

/**
 * A DTO for the Recipe entity.
 */
public class RecipeDTO implements Serializable {

    private Long id;

    private String name;

    private String instructions;

    private Set<IngredientItemDTO> ingredientItems = new HashSet<IngredientItemDTO>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public Set<IngredientItemDTO> getIngredientItems() {
        return ingredientItems;
    }

    public void setIngredientItems(Set<IngredientItemDTO> ingredientItems) {
        this.ingredientItems = ingredientItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RecipeDTO recipeDTO = (RecipeDTO) o;
        if(recipeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), recipeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RecipeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", instructions='" + getInstructions() + "'" +
            "}";
    }
}
