package com.surfalsur.resetario.service.impl;

import com.surfalsur.resetario.repository.IngredientItemRepository;
import com.surfalsur.resetario.service.RecipeService;
import com.surfalsur.resetario.domain.Recipe;
import com.surfalsur.resetario.repository.RecipeRepository;
import com.surfalsur.resetario.repository.search.RecipeSearchRepository;
import com.surfalsur.resetario.service.dto.RecipeDTO;
import com.surfalsur.resetario.service.mapper.RecipeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Recipe.
 */
@Service
@Transactional
public class RecipeServiceImpl implements RecipeService{

    private final Logger log = LoggerFactory.getLogger(RecipeServiceImpl.class);

    private final RecipeRepository recipeRepository;

    private final RecipeMapper recipeMapper;

    private final RecipeSearchRepository recipeSearchRepository;

    private final IngredientItemRepository ingredientItemRepository;

    public RecipeServiceImpl(RecipeRepository recipeRepository, RecipeMapper recipeMapper, RecipeSearchRepository recipeSearchRepository,  IngredientItemRepository ingredientItemRepository) {
        this.recipeRepository = recipeRepository;
        this.recipeMapper = recipeMapper;
        this.recipeSearchRepository = recipeSearchRepository;
        this.ingredientItemRepository = ingredientItemRepository;
    }

    /**
     * Save a recipe.
     *
     * @param recipeDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RecipeDTO save(RecipeDTO recipeDTO) {
        log.debug("Request to save Recipe : {}", recipeDTO);
        final Recipe recipe = recipeMapper.toEntity(recipeDTO);
        recipeRepository.save(recipe);

        recipe.getIngredientItems().forEach(it ->  {
            it.setRecipe(recipe);
            ingredientItemRepository.save(it);
        });

        RecipeDTO result = recipeMapper.toDto(recipe);
        recipeSearchRepository.save(recipe);
        return result;
    }

    /**
     *  Get all the recipes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RecipeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Recipes");
        return recipeRepository.findAll(pageable)
            .map(recipeMapper::toDto);
    }

    /**
     *  Get one recipe by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public RecipeDTO findOne(Long id) {
        log.debug("Request to get Recipe : {}", id);
        Recipe recipe = recipeRepository.findOne(id);
        return recipeMapper.toDto(recipe);
    }

    /**
     *  Delete the  recipe by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Recipe : {}", id);
        recipeRepository.delete(id);
        recipeSearchRepository.delete(id);
    }

    /**
     * Search for the recipe corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RecipeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Recipes for query {}", query);
        Page<Recipe> result = recipeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(recipeMapper::toDto);
    }
}
