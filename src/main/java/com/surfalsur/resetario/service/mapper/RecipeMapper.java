package com.surfalsur.resetario.service.mapper;

import com.surfalsur.resetario.domain.*;
import com.surfalsur.resetario.service.dto.IngredientItemDTO;
import com.surfalsur.resetario.service.dto.RecipeDTO;

import org.mapstruct.*;

import java.util.Set;

/**
 * Mapper for the entity Recipe and its DTO RecipeDTO.
 */
@Mapper(componentModel = "spring", uses = {IngredientItemMapper.class})
public interface RecipeMapper extends EntityMapper <RecipeDTO, Recipe> {


    @Mapping(source="ingredientItems",target = "ingredientItems")
    Recipe toEntity(RecipeDTO recipeDTO);


    @Mapping(source = "ingredientItems",target="ingredientItems")
    RecipeDTO toDto(Recipe entity);

    default Recipe fromId(Long id) {
        if (id == null) {
            return null;
        }
        Recipe recipe = new Recipe();
        recipe.setId(id);
        return recipe;
    }
}
