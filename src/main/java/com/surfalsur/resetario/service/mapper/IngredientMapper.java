package com.surfalsur.resetario.service.mapper;

import com.surfalsur.resetario.domain.*;
import com.surfalsur.resetario.service.dto.IngredientDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Ingredient and its DTO IngredientDTO.
 */
@Mapper(componentModel = "spring", uses = {CategoryMapper.class, })
public interface IngredientMapper extends EntityMapper <IngredientDTO, Ingredient> {

    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "category.name", target = "categoryName")
    IngredientDTO toDto(Ingredient ingredient); 

    @Mapping(source = "categoryId", target = "category")
    Ingredient toEntity(IngredientDTO ingredientDTO); 
    default Ingredient fromId(Long id) {
        if (id == null) {
            return null;
        }
        Ingredient ingredient = new Ingredient();
        ingredient.setId(id);
        return ingredient;
    }
}
