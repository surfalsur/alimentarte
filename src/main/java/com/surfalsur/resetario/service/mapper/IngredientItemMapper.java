package com.surfalsur.resetario.service.mapper;

import com.surfalsur.resetario.domain.*;
import com.surfalsur.resetario.service.dto.IngredientItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity IngredientItem and its DTO IngredientItemDTO.
 */
@Mapper(componentModel = "spring", uses = {IngredientMapper.class, RecipeMapper.class, })
public interface IngredientItemMapper extends EntityMapper <IngredientItemDTO, IngredientItem> {

    @Mapping(source = "ingredient.id", target = "ingredient.id")
    @Mapping(source = "ingredient.name", target = "ingredient.name")
    @Mapping(source = "ingredient.category.id", target = "ingredient.categoryId")
    @Mapping(source = "ingredient.category.name", target = "ingredient.categoryName")
    @Mapping(source = "recipe.id", target = "recipeId")
    @Mapping(source = "recipe.name", target = "recipeName")
    IngredientItemDTO toDto(IngredientItem ingredientItem);


    @Mapping(source = "ingredient.id", target = "ingredient.id")
    @Mapping(source = "ingredient.name", target = "ingredient.name")
    @Mapping(source = "ingredient.categoryId", target = "ingredient.category.id")
    @Mapping(source = "ingredient.categoryName", target = "ingredient.category.name")
    @Mapping(source = "recipeId", target = "recipe")
    IngredientItem toEntity(IngredientItemDTO ingredientItemDTO);

    default IngredientItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        IngredientItem ingredientItem = new IngredientItem();
        ingredientItem.setId(id);
        return ingredientItem;
    }
}
