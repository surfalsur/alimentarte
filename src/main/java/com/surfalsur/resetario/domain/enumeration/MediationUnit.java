package com.surfalsur.resetario.domain.enumeration;

/**
 * The MediationUnit enumeration.
 */
public enum MediationUnit {
    GRM {
        @Override
        public String getLabel() {
            return "Gramos";
        }


    }, KILOS {
        @Override
        public String getLabel() {
            return "Kilos";
        }


    }, LITROS {
        @Override
        public String getLabel() {
            return "Litros";
        }


    }, CM_CUBICOS {
        @Override
        public String getLabel() {
            return "Cm Cubicos";
        }


    }, UNIDADES {
        @Override
        public String getLabel() {
            return "Unidades";
        }


    }, CUCHARADAS {
        @Override
        public String getLabel() {
            return "Cucharadas";
        }
    };

    public abstract String getLabel();
}
