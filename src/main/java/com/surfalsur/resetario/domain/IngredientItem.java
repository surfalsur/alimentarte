package com.surfalsur.resetario.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.surfalsur.resetario.domain.enumeration.MediationUnit;

/**
 * A IngredientItem.
 */
@Entity
@Table(name = "ingredient_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "ingredientitem")
public class IngredientItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Min(value = 0)
    @Column(name = "quantity")
    private Integer quantity;

    @Enumerated(EnumType.STRING)
    @Column(name = "mediation_unit")
    private MediationUnit mediationUnit;

    @ManyToOne
    private Ingredient ingredient;

    @ManyToOne
    private Recipe recipe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public IngredientItem quantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public MediationUnit getMediationUnit() {
        return mediationUnit;
    }

    public IngredientItem mediationUnit(MediationUnit mediationUnit) {
        this.mediationUnit = mediationUnit;
        return this;
    }

    public void setMediationUnit(MediationUnit mediationUnit) {
        this.mediationUnit = mediationUnit;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public IngredientItem ingredient(Ingredient ingredient) {
        this.ingredient = ingredient;
        return this;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public IngredientItem recipe(Recipe recipe) {
        this.recipe = recipe;
        return this;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IngredientItem ingredientItem = (IngredientItem) o;
        if (ingredientItem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ingredientItem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IngredientItem{" +
            "id=" + getId() +
            ", quantity='" + getQuantity() + "'" +
            ", mediationUnit='" + getMediationUnit() + "'" +
            "}";
    }
}
