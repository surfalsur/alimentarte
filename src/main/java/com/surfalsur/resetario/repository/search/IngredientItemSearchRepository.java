package com.surfalsur.resetario.repository.search;

import com.surfalsur.resetario.domain.IngredientItem;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the IngredientItem entity.
 */
public interface IngredientItemSearchRepository extends ElasticsearchRepository<IngredientItem, Long> {
}
