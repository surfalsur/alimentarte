package com.surfalsur.resetario.repository;

import com.surfalsur.resetario.domain.Ingredient;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Ingredient entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IngredientRepository extends JpaRepository<Ingredient,Long> {
    
}
