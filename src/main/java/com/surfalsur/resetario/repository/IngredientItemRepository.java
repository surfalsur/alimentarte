package com.surfalsur.resetario.repository;

import com.surfalsur.resetario.domain.IngredientItem;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the IngredientItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IngredientItemRepository extends JpaRepository<IngredientItem,Long> {
    
}
